<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Requests\CountNumbersRequest;
use Illuminate\Support\Str;
use App\Http\Requests\IndexRequest;
use App\Http\Requests\MinRequest;
class ProblemSolvingController extends Controller
{

    public $apiResponse;

    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

   public function getCountNumbers(CountNumbersRequest $request)
   {
       $start=$request->start;
       $end=$request->end;
       $count=0;
       foreach (range($start,$end) as $num) 
       {
           if(Str::contains($num, '5')){
                continue;
           }

           $count++;
         
       }
       
       return $this->apiResponse->responseHandler(["status" => true, "message" => __("api.success"), "data" => $count]);
      
   }



   public function getIndexString(IndexRequest $request)
   {
      $result = 0;
      $input_string = $request->input_string;
      $chars=str_split(strtoupper($input_string));

      foreach($chars as $char)
      {
        $result *= 26;
        $firstArr=unpack('N', mb_convert_encoding($char, 'UCS-4BE', 'UTF-8'));
       
        $firstChar= $firstArr[1];

        $secondArr=unpack('N', mb_convert_encoding('A', 'UCS-4BE', 'UTF-8'));
       
        $secondChar= $secondArr[1];

        $result += $firstChar - $secondChar + 1;
      }

      return $this->apiResponse->responseHandler(["status" => true, "message" => __("api.success"), "data" => $result]);
      
   }

public function getMinSteps(MinRequest $request)
{
   
    // ans variable contains number of
    // operations processed after traversing
    // ith element
    $N = $request->N;
    $nums =  $request->Q;
    $ans = 0;
    $i = 0;
 
    // Traversing to get first non-zero element
    while ($i < $N && $nums[$i] == 0)
        $i++;
   
    // After getting first non-zero element we will
    // apply the given operation
    while ($i < $N - 1) {
        if ($nums[$i] == 0)
            $ans++;
        else
            $ans += $nums[$i];
        $i++;
    }
 
    // Returning the ans variable
    return $this->apiResponse->responseHandler(["status" => true, "message" => __("api.success"), "data" => $ans]);
}



}
