<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'N' => ['required','numeric'],
            'Q' => ['required','array'],
            'Q.*' => ['required','numeric'],

        ];
    }
}
