<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'problemSolving/', 'namespace' => 'Backend'], function () {

    Route::get("count-numbers", "ProblemSolvingController@getCountNumbers"); 
    Route::get("index-string", "ProblemSolvingController@getIndexString"); 
    Route::post("min-steps", "ProblemSolvingController@getMinSteps");

});

Route::group(['namespace' => 'Backend\User'], function () {


    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('add-items', 'UserController@apiAddItems');
   });

});
